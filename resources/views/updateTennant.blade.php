@extends('master')

@section('title')
    <title>Update Tennant</title>
@stop

@section('header')
    @include('header')
@stop

@section('content')
    <div class="container padding-top-10">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2>Update Tennant</h2>
            </div>
            <div class="panel-body">
                {!! Form::open(['url' => 'doUpdateTennant', 'method' => 'POST','role'=>'Form','data-toggle'=>'validator','files'=>true    ]) !!}
                    {{ csrf_field() }}
                    <input type="hidden" name="tennantId" id="tennantId">
                    <input type="hidden" name="mallId" id="mallId">
                    <input type="hidden" name="oldCategory" id="oldCategory">
                    <label for="tennantname" class="control-label">Tennant Name</label>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <input type="text" class="form-control" id="tennantname" name="tennantname" placeholder="Enter Tennant Name" required value="{!! old('tennantname') !!}">
                        </div>
                    </div>

                    <label for="tennantphone" class="control-label">Tennant Phone Number</label>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <input type="text" class="form-control" id="tennantphone" name="tennantphone" placeholder="Enter Tennant Phone (ex 021 - 56989999)" data-error="please insert the phone numbers"  required value="{!! old('mallphone') !!}">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <label for="location" class="control-label">Mall Location</label>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <select id="location" name="location" class="form-control" required disabled >
                                <option value="" disabled selected>Location</option>
                                <option value="Jakarta Utara">Jakarta Utara</option>
                                <option value="Jakarta Selatan">Jakarta Selatan</option>
                                <option value="Jakarta Timur">Jakarta Timur</option>
                                <option value="Jakarta Barat">Jakarta Barat</option>
                                <option value="Jakarta Pusat">Jakarta Pusat</option>
                                <option value="Bogor">Bogor</option>
                                <option value="Depok">Depok</option>
                                <option value="Tangerang">Tangerang</option>
                                <option value="Bekasi">Bekasi</option>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <label for="mallname" class="control-label">Mall Name</label>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <select id="mallname" name="mallname" class="form-control" required disabled>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <label for="categories" class="control-label">Categories</label>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <select id="categories" name="categories" class="form-control" required>
                                <option value="" disabled selected>categories</option>
                                @foreach($category as $category)
                                <option value="{!! $category->categoryName !!}">{!! $category->categoryName !!}</option>
                                    @endforeach
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <label for="tennantfloor" class="control-label">Tennant Level</label>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <select id="tennantfloor" name="tennantfloor" class="form-control" required>
                                <option value="" disabled selected>Tennant Level</option>
                                @foreach($floor as $floor)
                                    <option value="{!! $floor !!}">{!! $floor !!}</option>
                                @endforeach
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <label for="tennantlocation" class="control-label">Tennant Location</label>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <input type="text" class="form-control" id="tennantlocation" name="tennantlocation" placeholder="Enter Tennant Location" required value="{!! old('tennantlocation') !!}">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <label for="status" class="control-label">Tennant Status</label>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <select id="status" name="status" class="form-control" required>
                                <option value="" disabled selected>Status</option>
                                <option value="open">Open</option>
                                <option value="closed">Closed</option>
                                <option value="renovation">Renovation</option>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12">
                            <div>
                                <label for="uploadImage" class="control-label">Upload Photo</label>
                                <input type="file" name="uploadImage"  id="uploadImage">
                            </div>
                            </br>
                            <label for="Upload Preview" class="control-label">Upload Preview</label>
                            <div><img id="imgPreview" style="height: 40%; width: 40%;" src="#" hidden="hidden"/></div>
                        </div>
                    </div>

                    <div class="row padding-top-10">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('script')
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0])
            {
                var reader = new FileReader();
                reader.onload = function (e)
                {
                    $('#imgPreview').removeAttr('hidden','');
                    $('#imgPreview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $(document).ready(function(){
            $('#uploadImage').change(function()
            {
                readURL(this);
            });

        });
        $(document).ready(function(){

            /*
            * set value in update form
            * */
            $('#tennantname').val("{!! $tennant[0]->tennantName !!}");
            $('#tennantphone').val("{!! $tennant[0]->tennantPhone !!}");
            $('#tennantlocation').val("{!! $tennant[0]->tennantLocation  !!}");
            $('#tennantfloor').val("{!! $tennant[0]->tennantFloor !!}");
            $('#location').val("{!! $tennant[0]->mallId->mallLocation !!}");
            $('#mallname').append($('<option>', { value : "{!! $tennant[0]->mallId->objectId !!}" }).text("{!! $tennant[0]->mallId->mallName !!}"));
            $('#tennantId').val("{!! $tennant[0]->objectId !!}");
            $('#mallId').val("{!! $mallId !!}");
            $('#categories').val("{!! $tennant[0]->categoryId->categoryName !!}");
            $('#oldCategory').val("{!! $tennant[0]->categoryId->objectId !!}");
            $('#status').val("{!! $tennant[0]->tennantStatus !!}");
        });
    </script>
@stop


