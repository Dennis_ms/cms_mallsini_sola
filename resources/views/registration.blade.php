@extends('master')

@section('title')
    <title>Registration</title>
@stop

@section('header')
    @include('header')
@stop

@section('content')
    <div class="container padding-top-10">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2>Registration</h2>
            </div>
            <div class="panel-body">
                <form role="form" action="doRegister" method="post" id="register" data-toggle="validator">
                    {{ csrf_field() }}
                    <label for="newusername" class="control-label">Username</label>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <input type="text" class="form-control" id="newusername" name="newusername" placeholder="Username" required value="{!! old('newusername') !!}">
                        </div>
                    </div>
                    <label for="newemail" class="control-label">Email Address</label>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <input type="email" class="form-control" id="newemail" name="newemail" placeholder="Email address" data-error="Email address is invalid" required value="{!! old('newemail') !!}">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <label for="newpassword" class="control-label">Password</label>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <input type="password" data-minlength="6" class="form-control" id="newpassword" name="newpassword" placeholder="Enter your password" data-error="Minimum of 6 characters" required >
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <label for="newpasswordconfirm" class="control-label">Confirm Password</label>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <input type="password" class="form-control" id="newpasswordconfirm" name="newpasswordconfirm" placeholder="Confirm your password" data-match="#newpassword" data-error="Password doesn't match" required>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="row padding-top-10">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success">Register</button>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
