<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-COMPATIBLE" content="IE=edge">
    <meta name="author" content="Wendy">
    <link rel="icon" type="image/png" href="{{ asset('assets/mallsini_logo.png') }}" sizes="32x32">
    @yield('title')
    <!-- Bootstrap CSS -->
    @yield('css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link media="all" rel="stylesheet" href="{!! asset('bootstrap/css/bootstrap.min.css') !!}">
    <link media="all" rel="stylesheet" href="{!! asset('css/overdrive.css') !!}">
    <link media="all" rel="stylesheet" href="{!! asset('uikit/css/uikit.almost-flat.min.css') !!}">
    <link media="all" rel="stylesheet" href="{!! asset('selectize/css/selectize.bootstrap2.css') !!}" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src ="https://oss.maxcdn.com/html5shiv/3.7.2/html5shic.min.js"></script>
    <script src ="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

@yield('header')
<script>
    function logoff(){
        $.ajax({
            type: 'GET',
            url: '{!! url('logoff') !!}',
            success: function (msg)
            {
                location.reload();
            },
        });
    }
</script>
@yield('content')

<div class="uk-modal" id="popup-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <div id="popup-content">
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="{!! asset('bootstrap/js/bootstrap.min.js') !!}"></script>
<script src="{!! asset('moment/js/moment.js') !!}"></script>
<script src="{!! asset('uikit/js/uikit.min.js') !!}"></script>
<script src="{!! asset('bootstrap-validator/js/validator.min.js') !!}"></script>
<script src="{!! asset("uikit/js/components/lightbox.min.js") !!}"></script>
<script src="{!! asset("selectize/js/standalone/selectize.min.js") !!}"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

@yield('script')

<!-- google web fonts -->
<script>
    WebFontConfig = {
        google: {
            families: [
                'Source+Code+Pro:400,700:latin',
                'Roboto:400,300,500,700,400italic:latin'
            ]
        }
    };
    (function() {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
                '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })();
</script>

<script type="text/javascript">

        @if($errors->any())
        {
            UIkit.modal.alert("{!! $errors->first() !!}");
        }
        @endif

</script>

</body>
</html>