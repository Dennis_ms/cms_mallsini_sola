<nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="background-color: #ffa920">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-container">
                <span class="sr-only">Show and Hide the Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="navbar-container">
            @if(!empty(session('user')))
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/') }}" style="color: #FFFFFF"><span class="glyphicon glyphicon-home"></span> Home </a></li>
                    <li class="dropdown">
                        <a href="addMall" class="dropdown-toggle" data-toggle="dropdown" style="color: #FFFFFF"><span class="glyphicon glyphicon-tasks"></span> Mall <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('addMall') }}"><span class="glyphicon glyphicon-plus"></span> Insert</a></li>
                            <li><a href="{{ url('viewMall') }}"><span class="glyphicon glyphicon-edit"></span> Update</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="addTennant" class="dropdown-toggle" data-toggle="dropdown" style="color: #FFFFFF"><span class="glyphicon glyphicon-list"></span> Tennant <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('addTennant') }}"><span class="glyphicon glyphicon-plus"></span> Insert</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="addTennant" class="dropdown-toggle" data-toggle="dropdown" style="color: #FFFFFF"><span class="glyphicon glyphicon-star"></span> Promo <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('addPromo') }}"><span class="glyphicon glyphicon-plus"></span> Insert</a></li>
                            <li><a href="{{ url('viewPromo/1') }}"><span class="glyphicon glyphicon-edit"></span> Update</a></li>
                        </ul>
                    </li>
                    <li> <a href="#" style="color: #FFFFFF"> <span class="glyphicon glyphicon-user"></span> Welcome, {!! session('user')->email !!}</a></li>
                    <li> <button onclick="logoff()" style="margin-top: 7px; margin-left: 15px" type="button" class="btn btn-primary"> <span class="glyphicon glyphicon-log-out"></span> Logoff</button></li>
                </ul>

            @else
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/') }}"><span class="glyphicon glyphicon-home"></span> Home </a></li>
                    <li><a href="{{ url('/') }}"><span class="glyphicon glyphicon-book"></span> About Us </a></li>
                    <li class="dropdown">
                        <a href="addTennant" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-question-sign"></span> Contact Us <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#"><span class="glyphicon glyphicon-phone"></span> Phone</a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-comment"></span> Email</a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-envelope"></span> Mail</a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-map-marker"></span> Location</a></li>
                        </ul>
                    </li>
                </ul>
                {{--<li><a href="registration"><span class="glyphicon glyphicon-registration-mark"></span> Register</a></li>--}}
            <form class="navbar-form navbar-left" role="form" action="doLogin" method="post" id="login">
                {{ csrf_field() }}
                <div class="input-group">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-user"></span>
                            </span>
                    <input type="text" class="form-control input-sm" placeholder="Username" name="username" id="username"/>
                </div>
                <div class="input-group">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-lock"></span>
                            </span>
                    <input type="password" class="form-control input-sm" placeholder="Password" name="password" id="password"/>
                </div>
                <button type="submit" class="btn btn-success">Login</button>
            </form>
                @endif
        </div>
    </div>
</nav>
