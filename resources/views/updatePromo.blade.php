@extends('master')

@section('title')
    <title>Update Promo</title>
@stop

@section('header')
    @include('header')
@stop

@section('content')
    <div class="container padding-top-10">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2>Update Promo</h2>
            </div>
            <div class="panel-body">
                {!! Form::open(['url' => 'doUpdatePromo', 'method' => 'POST','role'=>'Form','data-toggle'=>'validator','files'=>true    ]) !!}
                {{ csrf_field() }}
                <label for="promotitle" class="control-label">Promo Title</label>
                <div class="row form-group">
                    <div class="col-md-12">
                        <input type="text" class="form-control" id="promotitle" name="promotitle" placeholder="Enter Title here" required value="{!! old('promotitle') !!}">
                    </div>
                </div>
                <input type="hidden" name="promoid" id="promoid">
                <label for="categories" class="control-label">Categories</label>
                <div class="row form-group">
                    <div class="col-md-12">
                        <select id="categories" name="categories" class="form-control" required>
                            <option value="" disabled selected>Categories</option>
                            @foreach($category as $category)
                                <option value="{!! $category->objectId !!}">{!! $category->categoryName !!}</option>
                            @endforeach
                        </select>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <label for="tennantname" class="control-label">Tennant Name</label>
                <div class="row form-group">
                    <div class="col-md-12">
                        <input type="text" class="form-control" id="tennantname" name="tennantname" placeholder="Enter Tennant Name" required >
                        <div class="help-block with-errors"></div>
                    </div>
                </div>



                <label for="promodesc" class="control-label">Promo Description</label>
                <div class="row form-group">
                    <div class="col-md-12">
                        <textarea rows="4" cols="50" class="form-control" id="promodesc" name="promodesc" placeholder="Promo Description" required></textarea>
                    </div>
                </div>

                <label for="floor" class="control-label">Select Mall Location</label>
                <div class="row form-group">
                    <div class="col-md-4">
                        <select id="mall" name="mallId[]" required multiple disabled>
                            <option value="" disabled selected>Pick Mall</option>
                            @foreach($promo[0]->mallId as $row)
                                    <option value="{!! $row->objectId !!}" selected>{!! $row->mallName !!}</option>
                            @endforeach
                        </select>
                        <div class="help-block with-errors"></div>
                    </div>

                </div>

                <label for="coordinates" class="control-label">Promo Dates</label>
                <div class="row form-group">
                    <div class="col-md-4">
                        <input id="datepicker_Start"  class="form-control" name="start_date" placeholder="Start Date"/>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-xs-1" style="padding-top: 5px"> ------------ </div>
                    <div class="col-md-4">
                        <input id="datepicker_End"  class="form-control" name="end_date" placeholder="End Date"/>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <label for="location" class="control-label">Credit Card</label>
                <div class="row form-group">
                    <div class="col-md-4">
                        <select id="creditcard" name="creditcard[]" disabled required multiple >
                            <option value="" disabled selected>Pick Credit Card</option>
                            @foreach($promo[0]->creditCardId as $row)
                                <option value="{!! $row->objectId !!}" selected>{!! $row->creditCardName !!}</option>
                            @endforeach
                        </select>

                    </div>
                </div>

                <label for="status" class="control-label">Promo Status</label>
                <div class="row form-group">
                    <div class="col-md-12">
                        <select id="promostatus" name="promostatus" class="form-control" required>
                            <option value="" disabled selected>Status</option>
                            <option value="Active">Active</option>
                            <option value="Expired">Expired</option>
                        </select>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-md-12">
                        <div>
                            <label for="uploadImage" class="control-label">Upload Icon</label>
                            <input type="file" name="uploadImageIcon" id="uploadImageIcon">
                        </div>
                        </br>
                        <label for="Upload Preview" class="control-label">Upload Preview</label>
                        <div><img id="imgPreview1" style="height: 40%; width: 40%;" src="#" hidden="hidden"/></div>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-md-12">
                        <div>
                            <label for="uploadImage" class="control-label">Upload Banner</label>
                            <input type="file" name="uploadImagePromo"  id="uploadImagePromo">
                        </div>
                        </br>
                        <label for="Upload Preview" class="control-label">Upload Preview</label>
                        <div><img id="imgPreview2" style="height: 40%; width: 40%;" src="#" hidden="hidden"/></div>
                    </div>
                </div>

                <div class="row padding-top-10">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('script')
    <script type="text/javascript">

        function readURL1(input) {
            if (input.files && input.files[0])
            {
                var reader = new FileReader();
                reader.onload = function (e)
                {
                    $('#imgPreview1').removeAttr('hidden','');
                    $('#imgPreview1').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        function readURL2(input) {
            if (input.files && input.files[0])
            {
                var reader = new FileReader();
                reader.onload = function (e)
                {
                    $('#imgPreview2').removeAttr('hidden','');
                    $('#imgPreview2').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        function convertSecondsToDate(secs) {

            var date = new Date(secs);
            return (date.getMonth()+1)+"/"+date.getDate()+"/"+date.getFullYear();
        }

        var availableTags=[];
        $(document).ready(function(){

            $( "#tennantname" ).autocomplete({
                source: availableTags
            });

            $( "#tennantname" ).keypress(function() {
                if($("#tennantname").val().length>3)
                {
                    $.ajax({
                        type: 'GET',
                        url: '{!! url('') !!}/getTennantName',
                        data: {tennantname: $('#tennantname').val()},
                        success: function (data) {
                            availableTags.length=0;
                            var data = JSON.parse(data);

                            for(var i =0;i<data.length;i++)
                            {
                                availableTags.push(data[i]);
                            }
                        }
                    });


                }
            });

            $('#promotitle').val("{!! $promo[0]->promoTitle !!}");
            $('#categories').val("{!! $promo[0]->promoCategory->objectId !!}");
            $('#tennantname').val("{!! $promo[0]->tennantName !!}");
            $('#promodesc').html("{!! $promo[0]->promoDescription !!}");
            $('#datepicker_Start').val(convertSecondsToDate({!! $promo[0]->promoStartDate !!}));
            $('#datepicker_End').val(convertSecondsToDate({!! $promo[0]->promoEndDate !!}));
            $('#promoid').val("{!! $promo[0]->objectId !!}");
            $('#promostatus').val("{!! $promo[0]->promoStatus !!}");

            $('#check_creditcard').on('change',function(){
                if ($('#check_creditcard').is(":checked"))
                {
                    var select = $("#creditcard").selectize();
                    var selectize = select[0].selectize;
                    selectize.clear();
                    selectize.disable();
                }
                else
                {
                    var select = $("#creditcard").selectize();
                    var selectize = select[0].selectize;
                    selectize.enable();
                }

            });


            $('#checkmall').on('change',function(){
                if ($('#checkmall').is(":checked"))
                {
                    var select = $("#mall").selectize();
                    var selectize = select[0].selectize;
                    selectize.clear();
                    selectize.disable();
                }
                else
                {
                    var select = $("#mall").selectize();
                    var selectize = select[0].selectize;
                    selectize.enable();
                }

            });



            $("#datepicker_Start").datepicker();
            $("#datepicker_End").datepicker();
            $("#mall").selectize({
                plugins: {
                    'remove_button': {
                        label     : ''
                    }
                }
            });
            
            $("#creditcard").selectize({
                plugins: {
                    'remove_button': {
                        label     : ''
                    }
                }
            });

            $('#uploadImageIcon').change(function()
            {
                readURL1(this);
            });

            $('#uploadImagePromo').change(function()
            {
                readURL2(this);
            });
        });
    </script>
@stop


