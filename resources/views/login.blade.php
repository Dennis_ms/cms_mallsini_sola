<!DOCTYPE html>
<html lang="en">
<style>
    body {
        background: url('{!! asset('assets/web_login_background.jpg') !!}') no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }
</style>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="{{ asset('assets/mallsini_logo.png') }}" sizes="32x32">

    <title>CMS MallSini</title>

    <!-- Bootstrap core CSS -->
    <link media="all" rel="stylesheet" href="{!! asset('bootstrap/css/bootstrap.min.css') !!}">

    <!-- Custom styles for this template -->
    <link media="all" rel="stylesheet" href="{!! asset('css/signin.css') !!}">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link media="all" rel="stylesheet" href="{!! asset('uikit/css/uikit.almost-flat.css') !!}">
</head>

<body>
<div class="container">
    <form class="form-signin" role="form" action="doLogin" method="post" id="login">
        {{ csrf_field() }}
        <img src="{!! asset('assets/logo.png') !!}" style="padding-left: 20px"/>
        <div class="col-md-12">
            <div class="col-md-4">
                <input type="email" id="inputEmail" class="form-control input-lg" placeholder="Email address" name="username" required autofocus>
            </div>
            <div class="col-md-4">
                <input type="password" id="inputPassword" class="form-control input-lg" placeholder="Password" name="password" required>
            </div>
            <div class="col-md-2">
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </div>
        </div>
        <img src="{!! asset('assets/copyright.png') !!}" style="padding-left: 30px"/>
    </form>

</div> <!-- /container -->


<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="{!! asset('bootstrap/js/bootstrap.min.js') !!}"></script>
<script src="{!! asset('moment/js/moment.js') !!}"></script>
<script src="{!! asset('uikit/js/uikit.min.js') !!}"></script>
<script src="{!! asset('bootstrap-validator/js/validator.min.js') !!}"></script>
<script src="{!! asset("uikit/js/components/lightbox.min.js") !!}"></script>
<script src="{!! asset("selectize/js/standalone/selectize.min.js") !!}"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript">
        @if($errors->any())
        {
        UIkit.modal.alert("{!! $errors->first() !!}");
    }
    @endif

</script>
