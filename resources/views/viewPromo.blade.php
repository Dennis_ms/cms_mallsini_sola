@extends('master')

@section('title')
    <title>Promo</title>
@stop

@section('header')
    @include('header')
@stop

@section('content')
    <div class="container padding-top-10">
        <div class="jumbotron">
            <table id="example" class="display" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Tennant</th>
                    <th>Promo Categories</th>
                    <th>Promo Title</th>
                    <th>Promo Icon</th>
                    <th>Promo Banner</th>
                    <th>Update</th>
                </tr>
                </thead>
                @foreach($promo as $row)
                    <tr>
                        <td>{!! $row['tennant'] !!}</td>
                        <td>{!! $row['categories'] !!}</td>
                        <td>{!! $row['title'] !!}</td>
                        <td>{!! $row['promoicon'] !!}</td>
                        <td>{!! $row['promopicture'] !!}</td>
                        <td>{!! $row['updatePromo'] !!}</td>
                    </tr>
                @endforeach
            </table>
            <ul class="pagination pagination-sm">
                @for($i=1;$i<=$total_pages;$i++)
                    <li><a href="{{ url('viewPromo/'.$i) }}" style="{{$pages+1 == $i ? 'background-color: #ffa920' : '' }}">{!! $i !!}</a></li>
                @endfor

            </ul>
        </div>
    </div>
@stop

@section('script')
    <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
    <link media="all" rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
    <link media="all" rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable({
                "pageLength":50,
                "lengthMenu":[25,50,100],
                "bPaginate":false,
                "bInfo":false
            });
        } );
    </script>
@stop


