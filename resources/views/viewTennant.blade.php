@extends('master')

@section('title')
    <title>Tennant</title>
@stop

@section('header')
    @include('header')
@stop

@section('content')
    <div class="container padding-top-10">
        <div class="jumbotron">
            <table id="example" class="display" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Tennant Name</th>
                    <th>Tennant Level</th>
                    <th>Tennant Location</th>
                    <th>Tennant Status</th>
                    <th>Categories</th>
                    <th>Tennant Image</th>
                    <th>Update Tennant</th>
                </tr>
                </thead>
                @foreach($data as $row)
                    <tr>
                        <td>{!! $row['tennantName'] !!}</td>
                        <td>{!! $row['tennantFloor']!!}</td>
                        <td>{!! $row['tennantLocation'] !!}</td>
                        <td>{!! $row['tennantStatus'] !!}</td>
                        <td>{!! $row['categories'] !!}</td>
                        <td>{!! $row['imageTennant'] !!}</td>
                        <td >{!! $row['updateTennant'] !!}</td>
                    </tr>
                @endforeach
            </table>
          <div>
              <ul class="pagination pagination-sm">


                  <li><a href="#" onclick="changeData(this)">A</a></li>
                  <li><a href="#" onclick="changeData(this)">B</a></li>
                  <li><a href="#" onclick="changeData(this)">C</a></li>
                  <li><a href="#" onclick="changeData(this)">D</a></li>
                  <li><a href="#" onclick="changeData(this)">E</a></li>
                  <li><a href="#" onclick="changeData(this)">F</a></li>
                  <li><a href="#" onclick="changeData(this)">G</a></li>
                  <li><a href="#" onclick="changeData(this)">H</a></li>
                  <li><a href="#" onclick="changeData(this)">I</a></li>
                  <li><a href="#" onclick="changeData(this)">J</a></li>
                  <li><a href="#" onclick="changeData(this)">K</a></li>
                  <li><a href="#" onclick="changeData(this)">L</a></li>
                  <li><a href="#" onclick="changeData(this)">M</a></li>
                  <li><a href="#" onclick="changeData(this)">N</a></li>
                  <li><a href="#" onclick="changeData(this)">O</a></li>
                  <li><a href="#" onclick="changeData(this)">P</a></li>
                  <li><a href="#" onclick="changeData(this)">Q</a></li>
                  <li><a href="#" onclick="changeData(this)">R</a></li>
                  <li><a href="#" onclick="changeData(this)">S</a></li>
                  <li><a href="#" onclick="changeData(this)">T</a></li>
                  <li><a href="#" onclick="changeData(this)">U</a></li>
                  <li><a href="#" onclick="changeData(this)">V</a></li>
                  <li><a href="#" onclick="changeData(this)">W</a></li>
                  <li><a href="#" onclick="changeData(this)">X</a></li>
                  <li><a href="#" onclick="changeData(this)">Y</a></li>
                  <li><a href="#" onclick="changeData(this)">Z</a></li>
                  <li><a href="#" onclick="changeData(this)">0</a></li>
                  <li><a href="#" onclick="changeData(this)">1</a></li>
                  <li><a href="#" onclick="changeData(this)">2</a></li>
                  <li><a href="#" onclick="changeData(this)">3</a></li>
                  <li><a href="#" onclick="changeData(this)">4</a></li>
                  <li><a href="#" onclick="changeData(this)">5</a></li>
                  <li><a href="#" onclick="changeData(this)">6</a></li>
                  <li><a href="#" onclick="changeData(this)">7</a></li>
                  <li><a href="#" onclick="changeData(this)">8</a></li>
                  <li><a href="#" onclick="changeData(this)">9</a></li>

              </ul>
          </div>
            <button class="btn btn-success">{!! link_to(URL::previous(), 'Back', ['class' => 'btn-success']) !!}</button>
        </div>
     </div>
@stop

@section('script')
    <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
    <link media="all" rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
    <link media="all" rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
        } );


        function changeData(object)
        {
            $.ajax({
               url:'{!! url("requestTennant") !!}',
                data:{'mallId':'{{$mallId}}','first':object.innerHTML},
                success:function(data)
                {
                    var data = JSON.parse(data);
                    $('#example').DataTable().destroy();
                    var table = $('#example').DataTable({

                        data: data,
                        columns: [
                            { data: 'tennantName'},
                            {data: 'tennantFloor' },
                            { data: 'tennantLocation'},
                            { data: 'tennantStatus'},
                            { data: 'categories' },
                            { data: 'imageTennant' },
                            { data: "updateTennant" }


                        ]
                    });
                }

            });
        }
    </script>

@stop


