@extends('master')

@section('title')
    <title>Mall</title>
@stop

@section('header')
    @include('header')
@stop

@section('content')
    <div class="container padding-top-10">
        <div class="jumbotron">
            <table id="example" class="display" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Mall Name</th>
                    <th>Mall Location</th>
                    <th>Mall Phone</th>
                    <th>Mall Status</th>
                    <th>Tennant List</th>
                    <th>Images</th>
                    <th>Update Mall</th>
                </tr>
                </thead>
                @foreach($data as $row)
                    <tr>
                        <td>{!! $row->mallName !!}</td>
                        <td>{!! $row->mallLocation !!}</td>
                        <td>{!! $row->mallPhone !!}</td>
                        <td>{!! $row->mallStatus !!}</td>
                        <td><a class="uk-button" href="viewTennant/{!! $row->objectId !!}/A">View Tennant</a> </td>
                        <td><a class="uk-button" href="{!! $row->mallPicture !!}"  class="uk-button" data-uk-lightbox>View Image</a></td>
                        <td><a class="uk-button" href="updateMall/{!! $row->objectId !!}">Update Data</a></td>
                    </tr>
                    @endforeach
            </table>
        </div>
    </div>
@stop

@section('script')
    <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
    <link media="all" rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
    <link media="all" rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
@stop


