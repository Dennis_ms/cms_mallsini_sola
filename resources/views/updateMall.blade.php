@extends('master')

@section('title')
    <title>Update Mall</title>
@stop

@section('header')
    @include('header')
@stop

@section('content')
    {{--{{dd($floor)}}}--}}
    <div class="container padding-top-10">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2>Update Mall</h2>
            </div>
            <div class="panel-body">
                {!! Form::open(['url' => 'doUpdateMall', 'method' => 'POST','role'=>'Form','data-toggle'=>'validator','files'=>true    ]) !!}
                    {{ csrf_field() }}
                    <input type="hidden" name="mallId" id="mallId">
                    <input type="hidden" name="coordinatesId" id="coordinatesId">
                    <label for="mallname" class="control-label">Mall Name</label>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <input type="text" class="form-control" id="mallname" name="mallname" placeholder="Enter Mall Name" required value="{!! old('mallname') !!}">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <label for="mallphone" class="control-label">Mall Phone Number</label>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <input type="text" class="form-control" id="mallphone" name="mallphone" placeholder="Enter Mall Phone (ex 021 - 56989999)" data-error="please insert the phone numbers" required value="{!! old('mallphone') !!}">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <label for="floor" class="uk-form-label">Mall Level</label>
                    <div class="row form-group" data-uk-grid-margin>
                        <div class="col-md-12">
                            <select id="floor" name="floor[]" required multiple>
                                <option value="" disabled selected>Pick Level</option>
                                <option value="LG" {{ in_array("LG", $floor ) ? 'selected' : '' }} >LG</option>
                                <option value="GF" {{ in_array("GF", $floor ) ? 'selected' : '' }} >GF</option>
                                <option value="UG" {{ in_array("UG", $floor ) ? 'selected' : '' }} >UG</option>
                                <option value="B"  {{ in_array("B", $floor ) ? 'selected' : '' }}  >B</option>
                                <option value="L1"  {{ in_array("L1", $floor ) ? 'selected' : '' }}  >1</option>
                                <option value="L2"  {{ in_array("L2", $floor ) ? 'selected' : '' }}  >2</option>
                                <option value="L3"  {{ in_array("L3", $floor ) ? 'selected' : '' }}  >3</option>
                                <option value="L4"  {{ in_array("L4", $floor ) ? 'selected' : '' }}  >4</option>
                                <option value="L5"  {{ in_array("L5", $floor ) ? 'selected' : '' }}  >5</option>
                                <option value="L6"  {{ in_array("L6", $floor ) ? 'selected' : '' }}  >6</option>
                                <option value="L7"  {{ in_array("L7", $floor ) ? 'selected' : '' }}  >7</option>
                                <option value="L8"  {{ in_array("L8", $floor ) ? 'selected' : '' }}  >8</option>
                            </select>
                        </div>
                    </div>

                    <label for="openhour" class="control-label">Open Hour</label>
                    <div class="row form-group">
                        <div class="col-md-4">
                            <select id="openhour" name="openhour" class="form-control" required>
                                <option value="" disabled selected>Open Hours</option>
                                <option value="1AM">1AM</option>
                                <option value="2AM">2AM</option>
                                <option value="3AM">3AM</option>
                                <option value="4AM">4AM</option>
                                <option value="5AM">5AM</option>
                                <option value="6AM">6AM</option>
                                <option value="7AM">7AM</option>
                                <option value="8AM">8AM</option>
                                <option value="9AM">9AM</option>
                                <option value="10AM">10AM</option>
                                <option value="11AM">11AM</option>
                                <option value="12AM">12AM</option>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="col-xs-1" style="padding-top: 5px"> ------------ </div>
                        <div class="col-md-4">
                            <select id="closehour" name="closehour" class="form-control" required>
                                <option value="" disabled selected>Close Hours</option>
                                <option value="1PM">1PM</option>
                                <option value="2PM">2PM</option>
                                <option value="3PM">3PM</option>
                                <option value="4PM">4PM</option>
                                <option value="5PM">5PM</option>
                                <option value="6PM">6PM</option>
                                <option value="7PM">7PM</option>
                                <option value="8PM">8PM</option>
                                <option value="9PM">9PM</option>
                                <option value="10PM">10PM</option>
                                <option value="11PM">11PM</option>
                                <option value="12PM">12PM</option>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <label for="latitude" class="control-label">Coordinates</label>
                    <div class="row form-group">
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="latitude" name="latitude" placeholder="Latitude (ex 6.2344123)" required value="{!! old('latitude') !!}">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="col-xs-1" style="padding-top: 5px"> ------------ </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="longitude" name="longitude" placeholder="Longitude (ex 106.2344123)"  required value="{!! old('longitude') !!}">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <label for="location" class="control-label">Mall Location</label>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <select id="location" name="location" class="form-control" required>
                                <option value="" disabled selected>Location</option>
                                <option value="Jakarta Utara">Jakarta Utara</option>
                                <option value="Jakarta Selatan">Jakarta Selatan</option>
                                <option value="Jakarta Timur">Jakarta Timur</option>
                                <option value="Jakarta Barat">Jakarta Barat</option>
                                <option value="Jakarta Pusat">Jakarta Pusat</option>
                                <option value="Bogor">Bogor</option>
                                <option value="Depok">Depok</option>
                                <option value="Tangerang">Tangerang</option>
                                <option value="Bekasi">Bekasi</option>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <label for="address" class="control-label">Mall Address</label>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <textarea rows="4" cols="50" class="form-control" id="address" name="address" placeholder="Mall Address" required></textarea>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <label for="status" class="control-label">Mall Status</label>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <select id="status" name="status" class="form-control" required>
                                <option value="" disabled selected>Status</option>
                                <option value="open">Open</option>
                                <option value="closed">Closed</option>
                                <option value="renovation">Renovation</option>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12">
                            <div>
                                <label for="uploadImage" class="control-label">Upload Photo</label>
                                <input type="file" name="uploadImage"  id="uploadImage">
                            </div>
                            </br>
                            <label for="Upload Preview" class="control-label">Upload Preview</label>
                            <div><img id="imgPreview" style="height: 40%; width: 40%;" src="#" hidden="hidden"/></div>
                        </div>
                    </div>

                    <div class="row padding-top-10">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('script')
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0])
            {
                var reader = new FileReader();
                reader.onload = function (e)
                {
                    $('#imgPreview').removeAttr('hidden','');
                    $('#imgPreview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $(document).ready(function(){

            $("#floor").selectize({
                plugins: {
                    'remove_button': {
                        label     : ''
                    }
                }
            });

            $('#status').val("{!! $mall[0]->mallStatus !!}");
            $('#mallId').val("{!! $mall[0]->objectId !!}");
            $('#mallname').val("{!! $mall[0]->mallName !!}");
            $('#mallphone').val("{!! $mall[0]->mallPhone !!}");
            $('#latitude').val("{!! $mall[0]->geoLocation->getLatitude() !!}");
            $('#longitude').val("{!! $mall[0]->geoLocation->getLongitude() !!}");
            $('#coordinatesId').val("{!! $mall[0]->geoLocation->getObjectId() !!}");
            var openhour="{!! $mall[0]->openHours !!}";
            var split=openhour.split("-");
            $('#openhour').val(split[0]);
            $('#closehour').val(split[1]);
            $('#location').val("{!! $mall[0]->mallLocation !!}");
            $('#address').val("{!! $mall[0]->mallAddress !!}");
            $('#uploadImage').change(function()
            {
                readURL(this);
            });

        });
    </script>
@stop


