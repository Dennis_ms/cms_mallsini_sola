<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
//  Login
    Route::get('/','LoginController@index');
    Route::get('logoff','LoginController@doLogoff');
    Route::post('doLogin','LoginController@doLogin');
    Route::get('dashboard','LoginController@dashboard');


//    Registration
//    Route::get('registration','MallSiniController@registration');
//    Route::post('doRegister','MallSiniController@doRegister');

//    Mall
    Route::get('addMall','MallController@addMall');
    Route::post('doAddMall','MallController@doAddMall');
    Route::get('getMallByLocation','MallController@getMallByLocation');
    Route::get('updateMall/{objectId}','MallController@updateMall');
    Route::get('updateMall','MallController@SearchMall');
    Route::post('doUpdateMall','MallController@doUpdateMall');
    Route::get('viewMall','MallController@viewMall');
    Route::get('getMallFloor','MallController@getMallFloor');

//    Tennant
    Route::get('addTennant','TennantController@addTennant');
    Route::post('doAddTennant','TennantController@doAddTennant');
    Route::get('getTennant','TennantController@getTennant');
    Route::get('updateTennant/{mallId}/{objectId}','TennantController@updateTennant');
    Route::post('doUpdateTennant','TennantController@doUpdateTennant');
    Route::get('viewTennant/{mallId}/{first}','TennantController@viewTennant');
    Route::get('requestTennant','TennantController@requestTennant');


//    Promo
    Route::get('addPromo','PromoController@addPromo');
    Route::post('doAddPromo','PromoController@doAddPromo');
    Route::get('viewPromo/{pages}','PromoController@viewPromo');
    Route::get('updatePromo/{promoId}','PromoController@updatePromo');
    Route::post('doUpdatePromo','PromoController@doUpdatePromo');
    Route::get('getTennantName','PromoController@getTennantName');

});

