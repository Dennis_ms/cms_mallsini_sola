<?php

namespace App\Http\Controllers;

use backendless\Backendless;
use backendless\services\persistence\BackendlessDataQuery;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Promo;
use Illuminate\Support\Facades\File;
use backendless\model\BackendlessCollection;

class PromoController extends Controller
{

    public function updatePromo($objectId)
    {
        if(session('user')==null)
        {
            return redirect('/');
        }
        $query= new BackendlessDataQuery();
        $query->setWhereClause("objectId like '".$objectId."'");
        $promo=Backendless::$Persistence->of('Promo')->find($query)->getAsObject();

        $query2= new BackendlessDataQuery();
        $query2->setPageSize(20);
        $category = Backendless::$Persistence->of('Category')->find($query2)->getAsObject();

        $promo[0]->promoDescription =str_replace("<br >",'&#13;&#10;', $promo[0]->promoDescription);
        $promo[0]->promoDescription =str_replace("\r\n",'&#13;&#10;', $promo[0]->promoDescription);
        return view('updatePromo',compact('promo','category'));
    }


    function randomString() {
        $length = 20;
        $str = "";
        $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }


    public function viewPromo($pages)
    {
        if(session('user')==null)
        {
            return redirect('/');
        }
//        $test= new BackendlessDataQuery();
//        $test_dump= ceil(Backendless::$Data->of('Category')->find($test)->totalObjectsCount()/2) ;
//        dd($test_dump);
//        $test_dump->loadNextPage();
        $pages=$pages-1;
        $offset=$pages*50;
        $query= new BackendlessDataQuery();
        $query->setPageSize(50);
        $query->setWhereClause("promoStatus ='Active' AND  promoStartDate <=". time()*1000 ." AND promoEndDate >= ".time()*1000 );
        $query->setOffset($offset);
        $data= Backendless::$Data->of('Promo')->find($query);
        $total_pages=ceil($data->totalObjectsCount()/50);
//
//        if($pages<$total_pages)
//        {
//            for($i=0;$i<$pages;$i++)
//            {
//                $data->loadNextPage();
//            }
//
//        }

//        dd($total_pages);
        $promo=array();
//        while($data->pageSize()>0)
//        {
            $object= $data->getAsObject();

            for($i=0;$i<count($object);$i++)
            {
                $temp=array();
                $temp['title']=$object[$i]->promoTitle;
                $temp['categories']=$object[$i]->promoCategory->categoryName;
                $temp['tennant']=$object[$i]->tennantName;
                $temp['promoicon']='<a class="uk-button" href="'. $object[$i]->promoIcon."\" class='uk-button' data-uk-lightbox>View Icon</a>";
                $temp['promopicture']='<a class="uk-button" href="'. $object[$i]->promoPicture."\" class='uk-button' data-uk-lightbox>View Banner</a>";
                $temp['updatePromo']='<a class="uk-button" href="'.url("updatePromo").'/'.$object[$i]->objectId.'">Update</a>';
                array_push($promo,$temp);
            }
//
//            $data->loadNextPage();
//            dd($promo);
//        }
        return view('viewPromo',compact('promo','total_pages','pages'));
    }
    public function addPromo()
    {
        if(session('user')==null)
        {
            return redirect('/');
        }
        $query= new BackendlessDataQuery();
        $query->setPageSize(100);
        $mall= Backendless::$Data->of('Mall')->find($query)->getAsObject();
        $credit_card=array();
        $get_credit_card=Backendless::$Data->of('CreditCard')->find($query);
        $category = Backendless::$Data->of('Category')->find($query)->getAsObject();
        while($get_credit_card->pageSize()>0)
        {
            $object= $get_credit_card->getAsObject();
            for($i=0;$i<$get_credit_card->pageSize();$i++)
            {
                array_push($credit_card,$object[$i]);
            }
            $get_credit_card->loadNextPage();
        }

        return view('addPromo', compact('credit_card','mall','category'));
    }

    public function doAddPromo(Request $request)
    {
        $promo = new Promo();
        if($request->checkmall==null)
        {
            $whereClauseMall="";
            for($i=0;$i<count($request->mallId);$i++)
            {
                if($i==0)
                {
                    $whereClauseMall="objectId Like '".$request->mallId[$i]."'";
                }
                else
                {
                    $whereClauseMall=$whereClauseMall." OR objectId Like '".$request->mallId[$i]."'";
                }
            }

            $query= new BackendlessDataQuery();
            $query->setWhereClause($whereClauseMall);
            $mall=Backendless::$Data->of('Mall')->find($query)->getAsObject();
        }
        else
        {
            $query= new BackendlessDataQuery();
            $query->setPageSize(100);
            $getMall=Backendless::$Data->of('Mall')->find($query);
            $mall= array();
            while($getMall->pageSize()>0)
            {
                $object= $getMall->getAsObject();
                for($i=0;$i<$getMall->pageSize();$i++)
                {
                 array_push($mall,$object[$i]);
                }
                $getMall->loadNextPage();
            }
        }

        $whereClauseCreditCard = "";
        for ($i = 0; $i < count($request->creditcard); $i++) {
            if ($i == 0) {
                $whereClauseCreditCard = "objectId Like '" . $request->creditcard[$i] . "'";
            } else {
                $whereClauseCreditCard = $whereClauseCreditCard . " OR objectId Like '" . $request->creditcard[$i] . "'";
            }
        }
        $query = new BackendlessDataQuery();
        $query->setWhereClause($whereClauseCreditCard);
        $creditcard = Backendless::$Data->of('CreditCard')->find($query)->getAsObject();


        $path = base_path()."/public/assets/promo";
        $promoname = $this->randomString();
        $promoicon = $this->randomString();
        if(!file_exists($path))
        {
            File::makeDirectory($path, 0755, true);
            $request->file('uploadImageIcon')->move($path,$promoicon.".".$request->file('uploadImageIcon')->getClientOriginalExtension());
            $request->file('uploadImagePromo')->move($path,$promoname.".".$request->file('uploadImagePromo')->getClientOriginalExtension());

        }
        else
        {
            $request->file('uploadImageIcon')->move($path,$promoicon.".".$request->file('uploadImageIcon')->getClientOriginalExtension());
            $request->file('uploadImagePromo')->move($path,$promoname.".".$request->file('uploadImagePromo')->getClientOriginalExtension());
        }
        $url_icon=asset("/assets/promo/".$promoicon.".".$request->file('uploadImageIcon')->getClientOriginalExtension());
        $url_promo=asset("/assets/promo/".$promoname.".".$request->file('uploadImagePromo')->getClientOriginalExtension());

        $queryGetCategory= new BackendlessDataQuery();
        $queryGetCategory->setWhereClause("objectId Like '".$request->categories."'");
        $category=Backendless::$Persistence->of('Category')->find($queryGetCategory)->getAsObject();

        $promo->setPromoCategory($category[0]);
        $promo->setTennantName($request->tennantname);
        $promo->setPromoDescription($request->promodesc);
        $promo->setPromoTitle($request->promotitle);
        $promo->setMallId($mall);
        $promo->setCreditCardId($creditcard);
        $promo->setPromoIcon($url_icon);
        $promo->setPromoPicture($url_promo);
        $promo->setPromoStartdate($request->start_date);
        $promo->setPromoEndDate($request->end_date);
        Backendless::$Persistence->save($promo);
        return redirect("addPromo")->withErrors("Insert new promo successful");
    }

    public function doUpdatePromo(Request $request)
    {
        $queryCategory= new BackendlessDataQuery();
        $queryCategory->setWhereClause("objectId like '".$request->categories."'");
        $category=Backendless::$Persistence->of('Category')->find($queryCategory)->getAsObject();

        $queryGetPromo= new BackendlessDataQuery();
        $queryGetPromo->setWhereClause("objectId Like '".$request->promoid."'");
        $promo=Backendless::$Data->of('Promo')->find($queryGetPromo)->getAsObject();

        $updatePromo = new Promo();
        $exp_promo=explode('/',$promo[0]->promoPicture);
        $exp_icon=explode('/',$promo[0]->promoIcon);
        if (!empty($request->file('uploadImagePromo')))
        {
            $path = base_path()."/public/assets/promo";
            $request->file('uploadImagePromo')->move($path,$exp_promo[count($exp_promo)-1]);
            $url_banner=asset("/assets/promo/".$exp_promo[count($exp_promo)-1]);
            $updatePromo->setPromoPicture($url_banner);

        }

        if(!empty($request->file('uploadImageIcon')))
        {
            $path = base_path()."/public/assets/promo";
            $request->file('uploadImageIcon')->move($path,$exp_icon[count($exp_icon)-1]);
            $url_icon=asset("/assets/promo/".$exp_icon[count($exp_icon)-1]);
            $updatePromo->setPromoIcon($url_icon);
        }

        $updatePromo->setPromoDescription($request->promodesc);
        $updatePromo->setPromoStartDate($request->start_date);
        $updatePromo->setPromoEndDate($request->end_date);
        $updatePromo->setTennantName($request->tennantname);
        $updatePromo->setPromoTitle($request->promotitle);
        $updatePromo->setPromoCategory($category[0]);
        $updatePromo->setObjectId($request->promoid);
        $updatePromo->setPromoStatus($request->promostatus);

        Backendless::$Persistence->update($updatePromo);

        return redirect('viewPromo/1')->withErrors("Promo has been updated");;
    }

        public function getTennantName(Request $request)
        {
            $query= new BackendlessDataQuery();
            $query->setWhereClause(" tennantname Like '%".$request->tennantname."%'");
            $query->setPageSize(100);
            $tennant= Backendless::$Data->of('Tennant')->find($query)->getAsObject();
            $name=array();
            for($i=0;$i<count($tennant);$i++)
            {
             array_push($name,$tennant[$i]->tennantName);
            }
            return json_encode($name);


        }


}
