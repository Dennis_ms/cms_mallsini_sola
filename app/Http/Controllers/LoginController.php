<?php namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use backendless\Backendless;
use backendless\exception\BackendlessException;

class LoginController extends Controller
{
    public function index()
    {
        if(session('user')!=null)
        {
            return redirect('dashboard');
        }
        return view('login');
    }

    public function dashboard()
    {
        if(session('user')==null)
        {
            return redirect('/');
        }
        return view('dashboard');
    }

    public function doLogin(Request $request)
    {
        try
        {
            Backendless::$UserService->login( $request->username, $request->password);
            $request->session()->put('user',Backendless::$UserService->getCurrentUser());
            return redirect('dashboard');
        }
        catch(BackendlessException $e)
        {
            return redirect('/')->withErrors($e->getMessage());
        }
    }

    public function doLogoff(Request $request)
    {
        if(!empty(session('user')))
        {
            Backendless::$UserService->setCurrentUser(session('user'));
        }
        Backendless::$UserService->logout(Backendless::$UserService->getCurrentUser());
        $request->session()->flush();
        return redirect("/");
    }

//    public function registration()
//    {
//        return view('registration');
//    }
//
//    public function doRegister(Request $request)
//    {
//
//        try
//        {
//
//            $user = new BackendlessUser();
//            $user->setName($request->newusername);
//            $user->setEmail($request->newemail);
//            $user->setPassword($request->newpassword);
//            Backendless::$UserService->register($user);
//            return redirect('/registration')->withErrors("Registration success please check email for account activation");
//        }
//        catch(BackendlessException $e)
//        {
//            return redirect('/')->withErrors($e->getMessage());
//        }
//    }

}