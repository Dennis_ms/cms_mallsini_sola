<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use backendless\services\persistence\BackendlessDataQuery;
use Illuminate\Http\Request;
use backendless\Backendless;
use App\Models\Tennant;
use App\Models\Mall;
use App\Models\Category;
use Closure;
use backendless\exception\BackendlessException;
use backendless\model\Data;
use Illuminate\Support\Facades\File;
class TennantController extends Controller
{

    function randomString()
    {
        $length = 20;
        $str = "";
        $characters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }

    public function addTennant()
    {
        if(session('user')==null)
        {
            return redirect('/');
        }
        $limit= new BackendlessDataQuery();
        $limit->setPageSize(50);
        $category= Backendless::$Persistence->of('Category')->find($limit)->getAsObject();
        return view('addTennant',compact('category'));
    }


    //--------------SAVE TENNANT---------------------
    public function doAddTennant(Request $request)
    {
        try
        {
            //---------GET MALL DATA FOR RELATIONS-----------------------
            $query = new BackendlessDataQuery();
            $query->setWhereClause("objectId LIKE '" . $request->mallname . "'");
            $mall = Backendless::$Data->of('Mall')->find($query)->getAsObject();
            //--------------UPLOAD IMAGES TO FOLDER ASSETS-----------------

            $tennantIcon = $this->randomString();
            $mallname = str_replace(" ","_",$mall[0]->mallName);

            $path = base_path()."/public/assets/" . $mallname;
            $request->file('uploadImage')->move($path,$tennantIcon.".".$request->file('uploadImage')->getClientOriginalExtension());
            $url=asset("/assets/".$mallname."/".$tennantIcon.".".$request->file('uploadImage')->getClientOriginalExtension());


            $query = new BackendlessDataQuery();
            $query->setWhereClause("categoryName Like '" . $request->categories . "'");
            $category = Backendless::$Data->of('Category')->find($query)->getAsObject();


            //-----------CREATE CLASS TENNANT--------------
            $tennant = new Tennant();
            //-----------SET TENNANT ATTRIBUTE VALUE----------
            $tennant->setTennantPicture($url);
            $tennant->setCategoryId($category[0]);
            $tennant->setTennantLocation($request->tennantlocation);
            $tennant->setTennantName($request->tennantname);
            $tennant->setTennantPhone($request->tennantphone);
            $tennant->setTennantFloor($request->tennantfloor);
            $tennant->setTennantPicture($url);
            $tennant->setMallId($mall[0]);
            $tennant->setTennantStatus($request->status);
            //--------------SAVE TENNANT---------------------
            Backendless::$Persistence->save($tennant);
            return redirect("addTennant")->withErrors("Insert tennant success");

        }
        catch(BackendlessException $e)
        {
            return redirect('addTennant')->withErrors($e->getMessage());
        }
    }

    protected function getTennantData($mallId,$first)
    {

        $data= array();
        $query=new BackendlessDataQuery();
        $query->setWhereClause(" mallId.objectId Like '$mallId' AND tennantName Like '$first%'");
        $query->setPageSize(100);
        $tennant=Backendless::$Persistence->of('Tennant')->find($query);
        while($tennant->pageSize()>0)
        {
            $object=$tennant->getAsArray();

            for($i=0;$i<$tennant->pageSize();$i++)
            {
                $object[$i]['categories']=$object[$i]['categoryId']['categoryName'];
                $object[$i]['updateTennant']='<a class="uk-button" href="'.url("updateTennant").'/'. $mallId.'/'.$object[$i]['objectId'].'">Update Data</a>';
                $object[$i]['imageTennant']='<a class="uk-button" href="'. $object[$i]['tennantPicture']."\" class='uk-button' data-uk-lightbox>View Image</a>";
                array_push($data,$object[$i]);
            }
            $tennant->loadNextPage();
        }

        return $data;
    }


    public function requestTennant(Request $request)
    {
        $data= $this->getTennantData($request->mallId,$request->first);

        return json_encode($data);

    }

    public function viewTennant($mallId)
    {
        if(session('user')==null)
        {
            return redirect('/');
        }
        $data=$this->getTennantData($mallId,'A');
        return view('viewTennant',compact('data','mallId'));
    }

    public function updateTennant($mallId,$objectId)
    {
        if(session('user')==null)
        {
            return redirect('/');
        }
        $query= new BackendlessDataQuery();
        $query->setWhereClause("objectId Like '".$objectId."'");
        $tennant = Backendless::$Persistence->of('Tennant')->find($query)->getAsObject();
        $limit= new BackendlessDataQuery();
        $limit->setPageSize(50);
        $category= Backendless::$Persistence->of('Category')->find($limit)->getAsObject();
        $floor=explode(',',$tennant[0]->mallId->mallFloor);
//        dd($tennant);
        return view('updateTennant',compact('tennant','category','floor','mallId'));
    }

    public function doUpdateTennant(Request $request)
    {
        try
        {
            //---------GET MALL DATA FOR RELATIONS-----------------------
            $query1 = new BackendlessDataQuery();
            $query1->setWhereClause("objectId LIKE '" . $request->mallId . "'");
            $mall = Backendless::$Data->of('Mall')->find($query1)->getAsObject();

            //--------------GET CATEGORY DATA FOR RELATIONS---------------------
            $query3 = new BackendlessDataQuery();
            $query3->setWhereClause("categoryName Like '" . $request->categories . "'");
            $category = Backendless::$Data->of('Category')->find($query3)->getAsObject();

            //-----------GET CLASS TENNANT--------------
            $query2 = new BackendlessDataQuery();
            $query2->setWhereClause("objectId Like '" . $request->tennantId . "'");
            Backendless::mapTableToClass("Tennant",'App\Models\Tennant');
            $tennant = Backendless::$Data->of('Tennant')->find($query2)->getAsClass();


            //--------------UPLOAD IMAGES TO FOLDER ASSETS-----------------
            if (!empty($request->file('uploadImage')))
            {
                $tennantIcon=explode('/',$tennant[0]->getTennantPicture());
                $mallname = str_replace(" ","_",$mall[0]->mallName);

                $path = base_path()."/public/assets/" . $mallname;
                $request->file('uploadImage')->move($path,$tennantIcon[count($tennantIcon)-1]);
                $url=asset("/assets/".$mallname."/".$tennantIcon[count($tennantIcon)-1]);
                $tennant[0]->setTennantPicture($url);
            }

            //-----------SET TENNANT ATTRIBUTE VALUE----------

            $tennant[0]->setTennantLocation($request->tennantlocation);
            $tennant[0]->setTennantName($request->tennantname);
            $tennant[0]->setTennantPhone($request->tennantphone);
            $tennant[0]->setTennantFloor($request->tennantfloor);
            $tennant[0]->setTennantStatus($request->status);
            $tennant[0]->setMallId($mall[0]);
            $tennant[0]->setCategoryId($category[0]);
            $tennant[0]->setObjectId($request->tennantId);
            Backendless::$Persistence->update($tennant[0]);

            return redirect("viewTennant/" . $request->mallId."/A")->withErrors("Tennant data has been updated");
        }
        catch(BackendlessException $e)
        {
            return redirect('updateTennant/'.$request->mallId."/".$request->tennantId)->withErrors($e->getMessage());
        }

    }
}
