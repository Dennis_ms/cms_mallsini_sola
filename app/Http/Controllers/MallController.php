<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Category;
use backendless\model\GeoPoint;
use backendless\services\persistence\BackendlessDataQuery;
use Illuminate\Http\Request;
use backendless\Backendless;
use backendless\exception\BackendlessException;
use App\Models\Mall;
use App\Models\Tennant;
use backendless\model\BackendlessCollection;
use Illuminate\Support\Facades\File;

class MallController extends Controller
{
    public function addMall()
    {
        if(session('user')==null)
        {
            return redirect('/');
        }
        return view('addMall');
    }

    public function UpdateMall($objectId)
    {
        if(session('user')==null)
        {
            return redirect('/');
        }
        $query= new BackendlessDataQuery();
        $query->setWhereClause("objectId Like '".$objectId."'");
        $mall= Backendless::$Data->of('Mall')->find($query)->getAsObject();
        $floor=explode(",",$mall[0]->mallFloor);

        return view('updateMall',compact('mall','floor'));

    }

    public function doUpdateMall(Request $request)
    {
        $query1 = new BackendlessDataQuery();
        $query1->setWhereClause("objectId LIKE '" . $request->mallId . "'");
        $oldmall = Backendless::$Data->of('Mall')->find($query1)->getAsObject();
        $oldmallname = str_replace(" ","_",$oldmall[0]->mallName);
        $newmallname = str_replace(" ","_",$request->mallname);
        $path = base_path()."/public/assets/" . $oldmallname;
        $newpath = base_path()."/public/assets/" . $newmallname;

        if(strcmp($oldmall[0]->mallName,$request->mallname)!== 0)
        {
            rename($path,$newpath);

            $query=new BackendlessDataQuery();
            $query->setWhereClause(" mallId.objectId Like '".$request->mallId."'");
            $tennant=Backendless::$Persistence->of('Tennant')->find($query);

            while($tennant->pageSize()>0)
            {
                $object=$tennant->getAsObject();
                for($i=0;$i<$tennant->pageSize();$i++)
                {

                    $urltennant = str_replace($oldmallname,$newmallname,$object[$i]->tennantPicture);
                    $newtennant = new Tennant();
                    $newtennant->setTennantPicture($urltennant);
                    Backendless::$Persistence->updateBulk($newtennant,"objectId Like '".$object[$i]->objectId."'");
                }
                $tennant->loadNextPage();
            }

        }

        $mall = new Mall();

        if (!empty($request->file('uploadImage')) && strcmp($oldmall[0]->mallName,$request->mallname) == 0)
        {
            $request->file('uploadImage')->move($path,$oldmallname.".".$request->file('uploadImage')->getClientOriginalExtension());
            $url=asset("/assets/".$oldmallname."/".$oldmallname.".".$request->file('uploadImage')->getClientOriginalExtension());
            $mall->setMallPicture($url);
        }
        else if(empty($request->file('uploadImage')) && strcmp($oldmall[0]->mallName,$request->mallname)!== 0)
        {
            $url=$oldmall[0]->mallPicture;
            $mall->setMallPicture($url);
        }
        else if(!empty($request->file('uploadImage')) && strcmp($oldmall[0]->mallName,$request->mallname)!== 0)
        {
            $request->file('uploadImage')->move($newpath,$newmallname.".".$request->file('uploadImage')->getClientOriginalExtension());
            $url=asset("/assets/".$newmallname."/".$newmallname.".".$request->file('uploadImage')->getClientOriginalExtension());
            $mall->setMallPicture($url);
        }



        $coordinates = new GeoPoint();
        $coordinates->setLatitude((float)$request->latitude);
        $coordinates->setlongitude((float) $request->longitude);
        $coordinates->setCategories('Mall');
        $coordinates->addMetadata('Mall',$request->mallname);
        $coordinates->setObjectId($request->coordinatesId);
        Backendless::$Geo->updatePoint($coordinates);

        $mall->setMallName($request->mallname);
        $mall->setMallPhone($request->mallphone);
        $mall->setOpenHours($request->openhour."-".$request->closehour);
        $mall->setMallLocation($request->location);
        $mall->setMallAddress($request->address);
        $floor=implode(',',$request->floor);
        $mall->setMallFloor($floor);
        $mall->setMallStatus($request->status);
        Backendless::$Persistence->updateBulk($mall,"objectId Like '".$request->mallId."'");

        return redirect('viewMall')->withErrors("Mall data has been updated");
    }

    public function doAddMall(Request $request)
    {
        try
        {
            $mallname = str_replace(" ","_",$request->mallname);
            $path = base_path()."/public/assets/" . $mallname;
            if(!file_exists($path))
            {
                File::makeDirectory($path, 0755, true);
                $request->file('uploadImage')->move($path,$mallname.".".$request->file('uploadImage')->getClientOriginalExtension());
            }
            else
            {
                $request->file('uploadImage')->move($path,$mallname.".".$request->file('uploadImage')->getClientOriginalExtension());
            }
            $url=asset("/assets/".$mallname."/".$mallname.".".$request->file('uploadImage')->getClientOriginalExtension());

            $coordinates = new GeoPoint();
            $coordinates->setLatitude((float)$request->latitude);
            $coordinates->setlongitude((float) $request->longitude);
            $coordinates->setCategories('Mall');
            $coordinates->addMetadata('Mall',$request->mallname);

            $newMall= new Mall();
            $newMall->setMallName($request->mallname);
            $newMall->setMallPhone($request->mallphone);
            $newMall->setOpenHours($request->openhour."-".$request->closehour);
            $newMall->setMallLocation($request->location);
            $newMall->setMallAddress($request->address);
            $newMall->setMallPicture($url);
            $newMall->setGeoLocation($coordinates);
            $newMall->setMallStatus($request->status);
            $floor=implode(',',$request->floor);
            $newMall->setMallFloor($floor);

            Backendless::$Persistence->save($newMall);
            return redirect("addMall")->withErrors("Mall data has been added");
            /*  rename File in backendless
                $save_image= Backendless::$Files->upload($file,"MallImage");
                $rename = Backendless::$Files->renameFile('/MallImage/'.Input::file('uploadImage')->getFileName(),$request->mallname.".JPG");
            */
        }
        catch(BackendlessException $e)
        {
            return redirect('addMall')->withErrors($e->getMessage())->withInput();;
        }

    }

    public function getMallByLocation(Request $request)
    {
        $query = new BackendlessDataQuery();
        $query->setWhereClause("mallLocation LIKE '$request->mallLocation' ");
        $query->setPageSize(50);
        $data=Backendless::$Data->of('Mall')->find($query)->getAsObject();
        return json_encode($data);
    }

    public function getMallByName(Request $request)
    {
        $query = new BackendlessDataQuery();
        $query->setWhereClause("objectId Like '".$request->objectId."'");
        $data= Backendless::$Data->of('Mall')->find($query)->getAsObject();

        return json_encode($data);
    }

    public function viewMall()
    {
        if(session('user')==null)
        {
            return redirect('/');
        }
        $data= array();
        $query = new BackendlessDataQuery();
        $query->setPageSize(100);
        $mall=Backendless::$Data->of('Mall')->find($query);
        while($mall->pageSize()>0)
        {
            $object=$mall->getAsObject();
            for($i=0;$i<$mall->pageSize();$i++)
            {
                array_push($data,$object[$i]);
            }
            $mall->loadNextPage();
        }
        return view('viewMall',compact('data'));
    }

    public function getMallFloor(Request $request)
    {
        $query=new BackendlessDataQuery();
        $query->setWhereClause("objectId Like '".$request->objectId."'");
        $data = Backendless::$Data->of('Mall')->find($query)->getAsObject();
        $floor=explode(",",$data[0]->mallFloor);

        return json_encode($floor);
    }


}
