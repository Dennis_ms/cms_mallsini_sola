<?php
/**
 * Created by PhpStorm.
 * User: Sola User 175
 * Date: 08/03/2016
 * Time: 11.45
 */

namespace App\Models;


class Category
{
    private $categoryName;

    /**
     * @return mixed
     */
    public function getCategoryName()
    {
        return $this->categoryName;
    }

    /**
     * @param mixed $categoryName
     */
    public function setCategoryName($categoryName)
    {
        $this->categoryName = $categoryName;
    }



}