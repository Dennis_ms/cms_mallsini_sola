<?php

namespace App\Models;


class Tennant
{
    private $objectId;
    private $tennantName;
    private $tennantLocation;
    private $tennantPhone;
    private $tennantPicture;
    private $tennantFloor;
    private $tennantStatus;
    private $categoryId;
    private $mallId;


//    public $__relations = [
//        'categoryId' => 'App\Models\Category',
//        'mallId' => 'App\Models\Mall'
//    ];


    public function getObjectId()
    {
        return $this->objectId;
    }

    public function setObjectId($objectId)
    {
        $this->objectId = $objectId;
    }

    public function getTennantName()
    {
        return $this->tennantName;
    }

    public function setTennantName($tennantName)
    {
        $this->tennantName = $tennantName;
    }

    public function getTennantLocation()
    {
        return $this->tennantLocation;
    }

    public function setTennantLocation($tennantLocation)
    {
        $this->tennantLocation = $tennantLocation;
    }

    public function getTennantPhone()
    {
        return $this->tennantPhone;
    }

    public function setTennantPhone($tennantPhone)
    {
        $this->tennantPhone = $tennantPhone;
    }

    public function getTennantPicture()
    {
        return $this->tennantPicture;
    }

    public function setTennantPicture($tennantPicture)
    {
        $this->tennantPicture = $tennantPicture;
    }

    public function getTennantFloor()
    {
        return $this->tennantFloor;
    }

    public function setTennantFloor($tennantFloor)
    {
        $this->tennantFloor = $tennantFloor;
    }

    public function getTennantStatus()
    {
        return $this->tennantStatus;
    }

    public function setTennantStatus($tennantStatus)
    {
        $this->tennantStatus = $tennantStatus;
    }

    public function getCategoryId()
    {
        return $this->categoryId;
    }

    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;
    }

    public function getMallId()
    {
        return $this->mallId;
    }

    public function setMallId($mallId)
    {
        $this->mallId = $mallId;
    }


}