<?php

namespace App\Models;

class Mall
{
    private $objectId;
    private $mallName;
    private $mallPhone;
    private $mallLocation;
    private $mallPicture;
    private $mallAddress;
    private $mallStatus;
    private $openHours;
    private $geoLocation;
    private $mallFloor;

    /**
     * @return mixed
     */
    public function getMallFloor()
    {
        return $this->mallFloor;
    }

    /**
     * @param mixed $mallFloor
     */
    public function setMallFloor($mallFloor)
    {
        $this->mallFloor = $mallFloor;
    }



    /**
     * @return mixed
     */
    public function getObjectId()
    {
        return $this->objectId;
    }

    /**
     * @param mixed $objectId
     */
    public function setObjectId($objectId)
    {
        $this->objectId = $objectId;
    }


    /**
     * @return mixed
     */
    public function getMallName()
    {
        return $this->mallName;
    }

    /**
     * @param mixed $mallName
     */
    public function setMallName($mallName)
    {
        $this->mallName = $mallName;
    }

    /**
     * @return mixed
     */
    public function getMallPhone()
    {
        return $this->mallPhone;
    }

    /**
     * @param mixed $mallPhone
     */
    public function setMallPhone($mallPhone)
    {
        $this->mallPhone = $mallPhone;
    }

    /**
     * @return mixed
     */
    public function getMallLocation()
    {
        return $this->mallLocation;
    }

    /**
     * @param mixed $mallLocation
     */
    public function setMallLocation($mallLocation)
    {
        $this->mallLocation = $mallLocation;
    }

    /**
     * @return mixed
     */
    public function getMallPicture()
    {
        return $this->mallPicture;
    }

    /**
     * @param mixed $mallPicture
     */
    public function setMallPicture($mallPicture)
    {
        $this->mallPicture = $mallPicture;
    }

    /**
     * @return mixed
     */
    public function getMallAddress()
    {
        return $this->mallAddress;
    }

    /**
     * @param mixed $mallAddress
     */
    public function setMallAddress($mallAddress)
    {
        $this->mallAddress = $mallAddress;
    }

    /**
     * @return mixed
     */
    public function getMallStatus()
    {
        return $this->mallStatus;
    }

    /**
     * @param mixed $mallStatus
     */
    public function setMallStatus($mallStatus)
    {
        $this->mallStatus = $mallStatus;
    }

    /**
     * @return mixed
     */
    public function getOpenHours()
    {
        return $this->openHours;
    }

    /**
     * @param mixed $openHours
     */
    public function setOpenHours($openHours)
    {
        $this->openHours = $openHours;
    }

    /**
     * @return mixed
     */
    public function getGeoLocation()
    {
        return $this->geoLocation;
    }

    /**
     * @param mixed $geoLocation
     */
    public function setGeoLocation($geoLocation)
    {
        $this->geoLocation = $geoLocation;
    }
}