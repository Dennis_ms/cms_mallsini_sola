<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class Promo
{
    private $creditCardId;
    private $mallId;
    private $promoCategory;
    private $tennantName;
    private $promoEndDate;
    private $promoPicture;
    private $promoTitle;
    private $promoIcon;
    private $objectId;
    private $promoStartdate;
    private $promoDescription;
    private $promoStatus;

    /**
     * Promo constructor.
     * @param $creditCardId
     */
    public function __construct()
    {
        $this->promoStatus = "Active" ;
    }

    /**
     * @return mixed
     */
    public function getPromoStatus()
    {
        return $this->promoStatus;
    }

    /**
     * @param mixed $promoStatus
     */
    public function setPromoStatus($promoStatus)
    {
        $this->promoStatus = $promoStatus;
    }

    /**
     * @return mixed
     */
    public function getObjectId()
    {
        return $this->objectId;
    }

    /**
     * @param mixed $objectId
     */
    public function setObjectId($objectId)
    {
        $this->objectId = $objectId;
    }

    /**
     * @return mixed
     */
    public function getCreditCardId()
    {
        return $this->creditCardId;
    }

    /**
     * @param mixed $creditCardId
     */
    public function setCreditCardId($creditCardId)
    {
        $this->creditCardId = $creditCardId;
    }

    /**
     * @return mixed
     */
    public function getMallId()
    {
        return $this->mallId;
    }

    /**
     * @param mixed $mallId
     */
    public function setMallId($mallId)
    {
        $this->mallId = $mallId;
    }

    /**
     * @return mixed
     */
    public function getPromoCategory()
    {
        return $this->promoCategory;
    }

    /**
     * @param mixed $promoCategory
     */
    public function setPromoCategory($promoCategory)
    {
        $this->promoCategory = $promoCategory;
    }

    /**
     * @return mixed
     */
    public function getTennantName()
    {
        return $this->tennantName;
    }

    /**
     * @param mixed $tennantName
     */
    public function setTennantName($tennantName)
    {
        $this->tennantName = $tennantName;
    }

    /**
     * @return mixed
     */
    public function getPromoEndDate()
    {
        return $this->promoEndDate;
    }

    /**
     * @param mixed $promoEndDate
     */
    public function setPromoEndDate($promoEndDate)
    {
        $this->promoEndDate = $promoEndDate;
    }

    /**
     * @return mixed
     */
    public function getPromoPicture()
    {
        return $this->promoPicture;
    }

    /**
     * @param mixed $promoPicture
     */
    public function setPromoPicture($promoPicture)
    {
        $this->promoPicture = $promoPicture;
    }

    /**
     * @return mixed
     */
    public function getPromoTitle()
    {
        return $this->promoTitle;
    }

    /**
     * @param mixed $promoTitle
     */
    public function setPromoTitle($promoTitle)
    {
        $this->promoTitle = $promoTitle;
    }

    /**
     * @return mixed
     */
    public function getPromoIcon()
    {
        return $this->promoIcon;
    }

    /**
     * @param mixed $promoIcon
     */
    public function setPromoIcon($promoIcon)
    {
        $this->promoIcon = $promoIcon;
    }

    /**
     * @return mixed
     */
    public function getPromoStartdate()
    {
        return $this->promoStartdate;
    }

    /**
     * @param mixed $promoStartdate
     */
    public function setPromoStartdate($promoStartdate)
    {
        $this->promoStartdate = $promoStartdate;
    }

    /**
     * @return mixed
     */
    public function getPromoDescription()
    {
        return $this->promoDescription;
    }

    /**
     * @param mixed $promoDescription
     */
    public function setPromoDescription($promoDescription)
    {
        $this->promoDescription = $promoDescription;
    }


}
