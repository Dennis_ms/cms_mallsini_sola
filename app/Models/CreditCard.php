<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CreditCard extends Model
{
    private $creditCardPicture;
    private $creditCardName;
    private $objectId;

    /**
     * @return mixed
     */
    public function getObjectId()
    {
        return $this->objectId;
    }

    /**
     * @param mixed $objectId
     */
    public function setObjectId($objectId)
    {
        $this->objectId = $objectId;
    }

    /**
     * @return mixed
     */
    public function getCreditCardPicture()
    {
        return $this->creditCardPicture;
    }

    /**
     * @param mixed $creditCardPicture
     */
    public function setCreditCardPicture($creditCardPicture)
    {
        $this->creditCardPicture = $creditCardPicture;
    }

    /**
     * @return mixed
     */
    public function getCreditCardName()
    {
        return $this->creditCardName;
    }

    /**
     * @param mixed $creditCardName
     */
    public function setCreditCardName($creditCardName)
    {
        $this->creditCardName = $creditCardName;
    }
}
